#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2
import re
from BeautifulSoup import BeautifulSoup

#shortdesc - описание блюда со ссылкой на рецепт, menu5 - меню справа, desc - собсно название рецепта + урл на него
classes = {'shortdesc': {'class': 'shortdesc'}, 'menu': {'class': 'menu5'}, 'desc_with_recipe_url': {'class': 'desc'}, 
'next_page': {'class': 'nav-next'}, 'all_ingrs': {'class': 'c8 ingredients'}, 'ingr': {'class': 'ingredient'}}
homepage = "http://www.say7.info"
all_recipes = "http://www.say7.info/cook/"

def get_urls(url, attr, urls = 0):  # возвращаем ссылки на все блюда со страницы
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page)
    menu_block = soup.findAll(attrs = attr, limit=urls)
    url_list = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', str(menu_block))
    return(url_list)

def get_ingr(url):                          # возвращаем строку - список ингредиентов
    page = urllib2.urlopen(url)
    soup = BeautifulSoup(page)
    ingr_class = soup.findAll(attrs = classes['ingr'])           # отбираем класс ingridient, содержащий список ингредиентов
    #ingr_class = BeautifulSoup(str(ingredient)).findAll('li')   # ещё можно выделить только теги со списком ингр., но не обязательно 
    ingreds = ''
    for ingred in ingr_class:
        ingreds = ingreds + ' ' + ingred.renderContents()             # вытаскиваем сами ингредиенты и собираем их в строку
    return(ingreds)
    
'''get_ingr(get_urls(all_recipes, classes['desc_with_recipe_url'])[0])'''


ingreds_all = ''
for url in get_urls(all_recipes, classes['desc_with_recipe_url']):
    ingreds_all = ingreds_all + ' ' + get_ingr(url)
    print ('processing ' + url)
print(ingreds_all)

#ingreds_all = '1 кг яблок (блабла) сахра сливочного яйцо ванилин'
ingr_list = {'сахар':'.*сахар', 'перец':'.*пер', 'сливки':'.*слив(ки|ок)', 'ваниль':'.*ванил', 'яйцо':'.*яй|иц'}
for ingr in ingr_list.keys():
    if re.search(ingr_list[ingr],ingreds_all):
        print('содержит ' + ingr)
    else:
        print('не содержит ' + ingr) 

recipe_list = all_recipes



'''while get_urls(recipe_list, classes['next_page']):
    recipe_list = get_urls(recipe_list, classes['next_page'], 1)[0]
    print (recipe_list)'''
        

''''if soup.findAll(attrs = {'class': 'shortdesc'}):
        get_urls()'''
    
